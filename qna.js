// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.
const { QnAMaker, LuisRecognizer } = require('botbuilder-ai');
const { MessageFactory, CardFactory } = require('botbuilder');
const util = require('util');
const fs = require('fs');
const collection = require('lodash/collection');
const { DbWrapper } = require('./db');

// Name or ID of the QnA Maker service in the .bot file.
const QNA_CONFIGURATION = '48' // RoamingKB // 'roaming_emp_qa_kb';
const LUIS_CONFIGURATION = '104' // TelefonicaRoaming // 'roaming_emp_qa_luis';

// CONSTS used in QnA Maker query. 
// See [here](https://docs.microsoft.com/en-us/azure/bot-service/bot-builder-howto-qna?view=azure-bot-service-4.0&tabs=cs) for additional info
const QNA_TOP_N = 1;
const QNA_CONFIDENCE_THRESHOLD = 0.5;

const intentsInfo = JSON.parse(fs.readFileSync('./resources/intentsInfo.json', 'utf8'));

const getQuickReplays = (intentName) => {
    const matchedIntent = collection.find(intentsInfo.intents, {name: intentName})
    if (!matchedIntent || !matchedIntent.quickReplays) return null;
    return MessageFactory.suggestedActions(matchedIntent.quickReplays.options, matchedIntent.quickReplays.question)
}

const getCardResponse = (intentName) => {
    const matchedIntent = collection.find(intentsInfo.intents, {name: intentName})
    if (!matchedIntent || !matchedIntent.cards) return null;
    const attachments = []
    matchedIntent.cards.attachments.forEach( (cardAttachment) => {
        attachments.push(CardFactory.adaptiveCard(cardAttachment))
    });
    return {
        text: matchedIntent.text,
        attachmentLayout: "carousel",
        attachments: attachments
    }
}

class QnA {
    /**
     *
     * @param {Object} botConfig bot configuration from .bot file
     */
    constructor(botConfig) {
        if (!botConfig) throw new Error('Need bot config');

        // add recognizers
        const qnaConfig = botConfig.findServiceByNameOrId(QNA_CONFIGURATION);
        if (!qnaConfig || !qnaConfig.kbId) throw new Error(`QnA Maker application information not found in .bot file. Please ensure you have all required QnA Maker applications created and available in the .bot file. See readme.md for additional information\n`);
        this.qnaRecognizer = new QnAMaker({
            knowledgeBaseId: qnaConfig.kbId,
            endpointKey: qnaConfig.endpointKey,
            host: qnaConfig.hostname
        });

        const luisConfig = botConfig.findServiceByNameOrId(LUIS_CONFIGURATION);
        if (!luisConfig || !luisConfig.appId) throw new Error(`qna LUIS model not found in .bot file. Please ensure you have all required LUIS models created and available in the .bot file. See readme.md for additional information\n`);
        this.luisRecognizer = new LuisRecognizer({
            applicationId: luisConfig.appId,
            azureRegion: luisConfig.region,
            // CAUTION: Its better to assign and use a subscription key instead of authoring key here.
            endpointKey: luisConfig.subscriptionKey
        });
        this.db = new DbWrapper()
    }
    /**
     *
     * @param {TurnContext} turn context object
     */
    async onTurn(turnContext) {
        // Call QnA Maker and get results.

        const luisResults = await this.luisRecognizer.recognize(turnContext);
        const topIntent = LuisRecognizer.topIntent(luisResults);

        console.log("topIntent", topIntent)

        const qnaResult = await this.qnaRecognizer.generateAnswer(topIntent.replace(/_/g, '.'), QNA_TOP_N, QNA_CONFIDENCE_THRESHOLD);
        const quickReplays = getQuickReplays(topIntent.replace(/_/g, '.'))

        console.log("qnaResult", qnaResult)
        
        if (!qnaResult || qnaResult.length === 0 || !qnaResult[0].answer) {
            await turnContext.sendActivities([
                { type: 'typing' },
                { type: 'delay', value: 1000 },
                { type: 'message', text: `¡Oh! Aún no conozco la respuesta a esa pregunta, pero puedes encontrar más información en [nuestro sitio web de roaming](https://ww2.movistar.cl/telefonia-movil/roaming/)` },
                { type: 'message', text: `También puedes comunicarte con nosotros y hablar con uno de nuestros ejecutivos llamando al 611 desde Chile o al +56991610611 desde el extranjero.` }
            ]);
            this.db.createMessage(turnContext, 'fallback', '¡Oh! Aún no conozco la respuesta a esa pregunta, pero puedes encontrar más información en [nuestro sitio web de roaming](https://ww2.movistar.cl/telefonia-movil/roaming/)<br>También puedes comunicarte con nosotros y hablar con uno de nuestros ejecutivos llamando al 611 desde Chile o al +56991610611 desde el extranjero.')
            return;
        } else {
            if (quickReplays) {
                await turnContext.sendActivity(quickReplays);
                this.db.createMessage(turnContext, topIntent, quickReplays.text, quickReplays.suggestedActions.actions)
            } else {

                const cards = getCardResponse(topIntent.replace(/_/g, '.'))

                if (cards) {
                    await turnContext.sendActivities([
                        { type: 'typing' },
                        { type: 'delay', value: 1000 },
                        { type: 'message', text: cards.text }
                    ]);
                    await turnContext.sendActivity(cards);
                    this.db.createMessage(turnContext, topIntent, cards.text, cards.attachments)
                } else {
                    const qnaAnswer = qnaResult[Math.floor(Math.random() * qnaResult.length)];
                    let messages = qnaAnswer.answer.split('<hr>')
                    if (messages.length > 1) {
                        this.db.createMessage(turnContext, topIntent, qnaAnswer.answer)
                        messages = messages.map((m) => {
                            return [
                                { type: 'typing' },
                                { type: 'delay', value: 2000 },
                                { type: 'message', text: m}
                            ]
                        })
                        messages.unshift([{ type: 'typing' }, { type: 'delay', value: 2000 }]);
                        const mergedMessages = [].concat(...messages);
                        await turnContext.sendActivities(mergedMessages);
                    } else {
                        this.db.createMessage(turnContext, topIntent, qnaAnswer.answer)
                        await turnContext.sendActivities([
                            { type: 'typing' },
                            { type: 'delay', value: 1000 },
                            { type: 'message', text: qnaAnswer.answer }
                        ]);
                    }
                }

            }
            return;
        }
    }
};

module.exports.QnA = QnA;
