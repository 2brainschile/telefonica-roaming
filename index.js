// index.js is used to setup and configure your bot

// Import required packages
const path = require('path');
const restify = require('restify');
const corsMiddleware = require('restify-cors-middleware');
const util = require('util');
const Raven = require('raven');

// Import required bot services. See https://aka.ms/bot-services to learn more about the different parts of a bot.
const { BotFrameworkAdapter, MemoryStorage, ConversationState, UserState } = require('botbuilder');
// Import required bot configuration.
const { BotConfiguration } = require('botframework-config');
const { CosmosDbStorage } = require('botbuilder-azure');

const { DispatchBot } = require('./bot');

// Read botFilePath and botFileSecret from .env file
// Note: Ensure you have a .env file and include botFilePath and botFileSecret.
const ENV_FILE = path.join(__dirname, '.env');
require('dotenv').config({ path: ENV_FILE });

Raven.config(process.env.sentryURL).install();

// Get the .bot file path
// See https://aka.ms/about-bot-file to learn more about .bot file its use and bot configuration.
const BOT_FILE = path.join(__dirname, (process.env.botFilePath || ''));
let botConfig;
try {
    // Read bot configuration from .bot file.
    botConfig = BotConfiguration.loadSync(BOT_FILE, process.env.botFileSecret);
} catch (err) {
    Raven.captureException(err);
    console.log(util.inspect(err))
    console.error(`\n Error reading bot file. Please ensure you have valid botFilePath and botFileSecret set for your environment.`);
    console.error(`\n - The botFileSecret is available under appsettings for your Azure Bot Service bot.`);
    console.error(`\n - If you are running this bot locally, consider adding a .env file with botFilePath and botFileSecret.`);
    console.error(`\n - See https://aka.ms/about-bot-file to learn more about .bot file its use and bot configuration.\n\n`);
    process.exit();
}

// For local development configuration as defined in .bot file
const DEV_ENVIRONMENT = 'development';

// bot name as defined in .bot file or from runtime
const BOT_CONFIGURATION = (process.env.NODE_ENV || DEV_ENVIRONMENT);

// Get bot endpoint configuration by service name
const endpointConfig = botConfig.findServiceByNameOrId(BOT_CONFIGURATION);

// Create adapter.
// See https://aka.ms/about-bot-adapter to learn more about .bot file its use and bot configuration .
const adapter = new BotFrameworkAdapter({
    appId: endpointConfig.appId || process.env.microsoftAppID,
    appPassword: endpointConfig.appPassword || process.env.microsoftAppPassword
});

// Catch-all for errors.
adapter.onTurnError = async (context, error) => {
    // This check writes out errors to console log
    // NOTE: In production environment, you should consider logging this to Azure
    //       application insights.
    console.error(`\n [onTurnError]: ${ error }`);
    // Send a message to the user
    await context.sendActivity(`¡Ay! No entiendo a qué te refieres. Para más información, visita [nuestro sitio web de roaming](https://ww2.movistar.cl/telefonia-movil/roaming/).`);
    // Load conversation state.
    await conversationState.load(context);
    // Clear out state.
    await conversationState.clear(context);
    // Save state changes.
    await conversationState.saveChanges(context);
};

// Define a state store for your bot. See https://aka.ms/about-bot-state to learn more about using MemoryStorage.
// A bot requires a state store to persist the dialog and user state between messages.
let conversationState, userState;

// For local development, in-memory storage is used.
// CAUTION: The Memory Storage used here is for local bot debugging only. When the bot
// is restarted, anything stored in memory will be gone.
 const memoryStorage = new MemoryStorage();

conversationState = new ConversationState(memoryStorage);
userState = new UserState(memoryStorage);

// Create the main dialog.
let bot;
try {
    bot = new DispatchBot(conversationState, userState, botConfig);
} catch (err) {
    Raven.captureException(err);
    console.error(`[botInitializationError]: ${ err }`);
    process.exit();
}

// Create HTTP server
let server = restify.createServer();
server.use(restify.plugins.bodyParser({"params":true}));
server.use(restify.plugins.queryParser());
// server.use(restify.fullResponse());

const cors = corsMiddleware({
  preflightMaxAge: 5,
  origins: ['*']
});

server.pre(cors.preflight);
server.use(cors.actual);

server.listen(process.env.port || process.env.PORT || 3978, function() {
    console.log(`\n${ server.name } listening to ${ server.url }`);
    console.log(`\nGet Bot Framework Emulator: https://aka.ms/botframework-emulator`);
    console.log(`\nPara hablar con el Bot, abrir el archivo .bot en el Emulador`);
});

const { DbWrapper } = require('./db');
const db = new DbWrapper()

server.get('/db/conversations', function(r, res, next) {
    db.getConversations(r.query.page).then( (req) => {
        res.contentType = 'json';
        res.send(200, req);
    }, (err) =>{
        console.log(err)
    })
});

server.get('/db/messages/:conversationId', function(r, res, next) {
    db.getMessagesInConversation(r.params.conversationId, r.query.page).then( (req) => {
        res.contentType = 'json';
        res.send(200, req.result.map((r) => r.document));
    }, (err) =>{
        console.log(err)
    })
});

server.get('/db/statistics', function(r, res, next) {
    db.getStatistics().then( (req) => {
        res.contentType = 'json';
        const statistics = {}
        req.map((r) => statistics[Object.keys(r)[0]] = r[Object.keys(r)[0]])
        res.send(200, statistics);
    }, (err) =>{
        console.log(err)
    })
});

// Listen for incoming activities and route them to your bot main dialog.
server.post('/api/messages', (req, res) => {
    // Route received a request to adapter for processing
    adapter.processActivity(req, res, async (turnContext) => {
        // route to bot activity handler.
        await bot.onTurn(turnContext);
    });
});

server.get('/*', restify.plugins.serveStatic({
  directory: './public',
  default: 'index.html'
}))

