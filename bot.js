// bot.js
// 
const { LuisRecognizer } = require('botbuilder-ai');
const { ActivityTypes, MessageFactory } = require('botbuilder');
const { QnA } = require('./qna');
const { SmalltalkQnA } = require('./smalltalk-qna');
const { DbWrapper } = require('./db');
const util = require('util');

// this is the LUIS service type entry in the .bot file.
const DISPATCH_CONFIG = '138'// 'TelefonicaRoamingDispatch' // 'roaming_dispatch_emp_qa_luis';

const QNA_INTENT = 'l_TelefonicaRoaming';//'l_MovistarQnA';

class DispatchBot {
    /**
     * @param {ConversationState}  conversation state
     * @param {UserState} user state
     * @param {BotConfiguration} bot configuration from .bot file
     */
    constructor(conversationState, userState, botConfig) {
        if (!conversationState) throw new Error(`Missing parameter. Conversation state parameter is missing`);
        if (!userState) throw new Error(`Missing parameter. User state parameter is missing`);
        if (!botConfig) throw new Error(`Missing parameter. Bot configuration parameter is missing`);

        this.qnaDialog = new QnA(botConfig);
        this.smalltalkDialog = new SmalltalkQnA(botConfig)
        this.db = new DbWrapper()

        this.conversationState = conversationState;
        this.userState = userState;

        // dispatch recognizer
        const dispatchConfig = botConfig.findServiceByNameOrId(DISPATCH_CONFIG);
        if (!dispatchConfig || !dispatchConfig.appId) throw new Error(`No dispatch model found in .bot file. Please ensure you have dispatch model created and available in the .bot file. See readme.md for additional information\n`);
        this.dispatchRecognizer = new LuisRecognizer({
            applicationId: dispatchConfig.appId.replace(/(â|€|©)*/g, ""),
            endpoint: dispatchConfig.getEndpoint(),
            endpointKey: dispatchConfig.subscriptionKey
        });
    }

    /**
     * Driver code that does one of the following:
     * 1. Calls dispatch LUIS model to determine intent
     * 2. Calls appropriate sub component to drive the conversation forward.
     *
     * @param {TurnContext} context turn context from the adapter
     */
    async onTurn(turnContext) {
        if (turnContext.activity.type === ActivityTypes.Message) {
            // determine which dialog should fulfill this request
            // call the dispatch LUIS model to get results.
            // 
            const dispatchResults = await this.dispatchRecognizer.recognize(turnContext);
            const dispatchTopIntent = LuisRecognizer.topIntent(dispatchResults);
            
            // Save user intent to db
            this.db.createConversation(turnContext)
            this.db.createMessage(turnContext, false, turnContext.activity.text)

            console.log(dispatchTopIntent)
            switch (dispatchTopIntent) {
            case QNA_INTENT:
                await this.qnaDialog.onTurn(turnContext);
                break;
            default:
                await this.smalltalkDialog.onTurn(turnContext);
            }

            // save state changes
            await this.conversationState.saveChanges(turnContext);
            await this.userState.saveChanges(turnContext);

        } else if (turnContext.activity.type === ActivityTypes.ConversationUpdate) {
            // Handle ConversationUpdate activity type, which is used to indicates new members add to
            // the conversation.
            // see https://aka.ms/about-bot-activity-message to learn more about the message and other activity types

            // Do we have any new members added to the conversation?
            if (turnContext.activity.membersAdded.length !== 0) {
                // Iterate over all new members added to the conversation
                for (var idx in turnContext.activity.membersAdded) {
                    // Greet anyone that was not the target (recipient) of this message
                    // the 'bot' is the recipient for events from the channel,
                    // turnContext.activity.membersAdded == turnContext.activity.recipient.Id indicates the
                    // bot was added to the conversation.
                    if (turnContext.activity.membersAdded[idx].id !== turnContext.activity.recipient.id) {
                        // Welcome user.
                        // When activity type is "conversationUpdate" and the member joining the conversation is the bot
                        this.db.createMessage(turnContext, 'welcome', '¡Hola! Soy el nuevo Asistente Digital de Movistar especialista en Roaming. ¿En qué puedo ayudarte?')

                        await turnContext.sendActivities([ // `¡Hola! Soy el nuevo Asistente Digital de Movistar Roaming. ¿En qué puedo ayudarte?`);
                            { type: 'typing' },
                            { type: 'delay', value: 1000 },
                            { type: 'message', text: '¡Hola! Soy el nuevo Asistente Digital de Movistar especialista en Roaming.' },
                            { type: 'typing' },
                            { type: 'delay', value: 2000 },
                            { type: 'message', text: '¿En qué puedo ayudarte?' }
                        ])
                    }
                }
            }
        }
    }
}

module.exports.DispatchBot = DispatchBot;
