// load_xlsx.js is used to load intents from an xlsx into luis.ai

const util = require('util');
const fs = require('fs');
const path = require('path');
const exec = util.promisify(require('child_process').exec);
const { BotConfiguration } = require('botframework-config');

require('dotenv').config({ path: ENV_FILE });

const ENV_FILE = path.join(__dirname, '.env');
const BOT_FILE = path.join(__dirname, (process.env.botFilePath || ''));
const SMALLTALK_PATH = "resources/luis/Smalltalk Telefonica Roaming.xlsx"
const FAQ_PATH = "resources/luis/FAQ Movistar Roaming - Intents.xlsx";

let botConfig;
try {
    // Read bot configuration from .bot file.
    botConfig = BotConfiguration.loadSync(BOT_FILE, process.env.botFileSecret);
} catch (err) {
    console.log(util.inspect(err))
    console.error(`\n Error reading bot file. Please ensure you have valid botFilePath and botFileSecret set for your environment.`);
    console.error(`\n - The botFileSecret is available under appsettings for your Azure Bot Service bot.`);
    console.error(`\n - If you are running this bot locally, consider adding a .env file with botFilePath and botFileSecret.`);
    console.error(`\n - See https://aka.ms/about-bot-file to learn more about .bot file its use and bot configuration.\n\n`);
    process.exit();
}

// Get bot endpoint configuration by service id
const smalltalkConfig = botConfig.findServiceByNameOrId('34');
const faqConfig = botConfig.findServiceByNameOrId('104');
const dispatchConfig = botConfig.findServiceByNameOrId('138');

async function updateLuis(appId, authoringKey, appFileName) {
  console.log('running updateLuis for', appFileName);
  const { stdout, stderr } = await exec(`./node_modules/@telefonica/luis-cli/bin/luis-cli update -s ${authoringKey} -a ${appId} -m cognitiveModels/${appFileName}.luis -v 0.1`)
  console.log('updateLuis stdout:', stdout);
  console.log('updateLuis stderr:', stderr);
  console.log('updateLuis DONE')
}

async function createLuisJson(luFileName, appName) {
  const culture = 'es-mx' // luFileName == 'smalltalk' ? 'en-us' : 'es-mx'
  const { stdout, stderr } = await exec(`ludown parse toluis --in cognitiveModels/${luFileName}.lu -o cognitiveModels --out ${luFileName}.luis -n ${appName} -c ${culture} --verbose`);
  console.log('createLuisJson stdout:', stdout);
  console.log('createLuisJson stderr:', stderr);
  console.log('createLuisJson DONE')
}

if(typeof require !== 'undefined') XLSX = require('xlsx');

const generateLuFile = (sheet, luFileName) => {
  let inputs = []
  return new Promise( (resolve) => {
    const stream = fs.createWriteStream(`cognitiveModels/${luFileName}.lu`, {'encoding':'utf8'});
    stream.once('open', function(fd) {
      const rows = XLSX.utils.sheet_to_json(sheet)
      stream.write("\n> # Intent definitions\n")
      stream.write("\n## None\n");
      rows.forEach((row) => {
        const intentInfo = Object.values(row);
        stream.write("\n## " + intentInfo[0]);
        intentInfo.shift()
        intentInfo.forEach( (intentInput) => {
          stream.write("\n- " +intentInput)
          inputs.push(intentInput)
        })
        stream.write("\n")
      })
      stream.end();
      resolve(inputs)
    });
  })
}

function generateDispatchLuFile (smalltalkUtterances, faqsUtterances) {
  return new Promise( (resolve) =>{
    const stream = fs.createWriteStream(`cognitiveModels/dispatch.lu`, {'encoding':'utf8'});
    stream.once('open', function(fd) {
      stream.write("\n> # Intent definitions\n")
      stream.write("\n## None\n");
      stream.write("\n## l_TelefonicaRoaming");
      faqsUtterances.forEach((row) => {
        stream.write("\n- " + row)
      })
      stream.write("\n")
      stream.write("\n## l_MovistarSmalltalk");
      smalltalkUtterances.forEach((row) => {
        stream.write("\n- " + row)
      })
      stream.end();
      resolve()
    });
  })
}

// Generate lu files for smalltalk and faq
const smalltalkWorkbook = XLSX.readFile(path.join(__dirname, SMALLTALK_PATH));
const faqWorkbook = XLSX.readFile(path.join(__dirname, FAQ_PATH));

const updateSmalltalk = generateLuFile(smalltalkWorkbook.Sheets['inputs'], 'smalltalk')
  .then( (smalltalkInputs) => {
    createLuisJson('smalltalk', smalltalkConfig.name)
      .then( () => updateLuis(smalltalkConfig.appId, smalltalkConfig.authoringKey, 'smalltalk'))
    return smalltalkInputs
  })

const updateFaq = generateLuFile(faqWorkbook.Sheets['inputs'], 'faq')
  .then( (faqInputs) => {
    createLuisJson('faq', faqConfig.name)
      .then( () => updateLuis(faqConfig.appId, faqConfig.authoringKey, 'faq') )
    return faqInputs
  })


async function updateSmalltalkAndFaq() {
 const smalltalkUtterances = await updateSmalltalk
 const faqUtterances = await updateFaq
 return { smalltalkUtterances, faqUtterances}
}

// update luis in each app
updateSmalltalkAndFaq()
  // Create dispatch lu file
  .then( (obj) => generateDispatchLuFile(obj.smalltalkUtterances, obj.faqUtterances) )
  // update dispatch app
  .then( () => createLuisJson('dispatch', dispatchConfig.name) )
  // update dispatch luis
  .then( () => updateLuis(dispatchConfig.appId, dispatchConfig.authoringKey, 'dispatch') )
