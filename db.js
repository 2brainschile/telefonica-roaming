const path = require('path');
const dotEnv = require('dotenv');
const { CosmosDbStorage } = require('botbuilder-azure');
const CosmosClient = require('@azure/cosmos').CosmosClient;

const ENV_FILE = path.join(__dirname, '.env');

const CHATROOMS_CONTAINER = 'bot-chatrooms'
const MESSAGES_CONTAINER = 'bot-messages'

const MONTHS = [
    "Ene",
    "Feb",
    "Mar",
    "Abr",
    "May",
    "Jun",
    "Jul",
    "Ago",
    "Sep",
    "Oct",
    "Nov",
    "Dic"
]

function getLastSixMonths() {
    let months = [...Array(6).keys()]
    let date = new Date()
    months.forEach( (m, i) => {
        let monthNumber = date.getMonth() + 1
        monthNumber = monthNumber < 10 ? ('0' + monthNumber.toString()) : monthNumber
        months[i] = `${date.getFullYear()}-${monthNumber}`;
        date.setMonth(date.getMonth() - 1); 
    })
    return months.reverse();
}

class DbWrapper {
    /**
     *
     * @param {Object} botConfig bot configuration from .bot file
     */
    constructor(botConfig) {
        dotEnv.config({ path: ENV_FILE });

        if (!process.env.cosmosDbHost) throw new Error('Need db config');

        this.conversationStorage = new CosmosDbStorage({
            serviceEndpoint: process.env.cosmosDbHost, 
            authKey: process.env.cosmosDbKey, 
            databaseId: process.env.cosmosDbName,
            collectionId: CHATROOMS_CONTAINER
        })
        this.messageStorage = new CosmosDbStorage({
            serviceEndpoint: process.env.cosmosDbHost, 
            authKey: process.env.cosmosDbKey, 
            databaseId: process.env.cosmosDbName,
            collectionId: MESSAGES_CONTAINER
        })
        this.cosmosClient = new CosmosClient({
            endpoint: process.env.cosmosDbHost,
            auth: { masterKey: process.env.cosmosDbKey } 
        });

    }

    /**
     *
     * @param {TurnContext} turn context object
     */
    createConversation(turnContext) {
        const conversationInfo = {}
        conversationInfo[turnContext.activity.conversation.id] = JSON.parse(JSON.stringify(turnContext.activity));
        delete conversationInfo[turnContext.activity.conversation.id].text
        this.conversationStorage.write(conversationInfo)
    }
    /**
     *
     * @param {TurnContext} turn context object
     */
    createMessage(turnContext, intentName, text, quickReplays) {
        const messageInfo = {}
        messageInfo[turnContext.activity.id] = turnContext.activity;
        if (intentName) { // bot response
            messageInfo[turnContext.activity.id].intent = intentName;
            messageInfo[turnContext.activity.id].text = text;
            if (quickReplays) {
                messageInfo[turnContext.activity.id].quickReplays = quickReplays;
            }
        } else { // user response
            messageInfo[turnContext.activity.id].userSays = text;
        }
        this.messageStorage.write(messageInfo)
    }

    /**
    * Read the database and get all conversations paginated
    */
    getConversations(page) {
        const vm = this
        let perPage = 20
        
        const querySpec = {
            query: "SELECT TOP " + ((page || 1) * perPage) + " * FROM c ORDER BY c._ts DESC",
            parameters: []
        };

        return new Promise((resolve, reject) => {
            vm.cosmosClient.database(process.env.cosmosDbName)
                .container(CHATROOMS_CONTAINER)
                .items
                .query(querySpec, {enableCrossPartitionQuery: true})
                .toArray()
                .then( (req) => {
                    const formmatedResult = []
                    
                    req.result.slice(((page || 1) * perPage) - perPage).forEach((r) => {
                        formmatedResult.push(r.document)
                    })
                    resolve(formmatedResult);
                }, (err) =>{
                    reject(err)
                })
        })
    }

    /**
    * Read the database and get all messages in Conversation
    */
    getMessagesInConversation(conversationId, page) {

       let perPage = 10
        const querySpec = {
            query: "SELECT TOP " + ((page || 1) * perPage) + 
                    " * FROM c WHERE c.document.conversation.id = '" + conversationId + 
                    "' ORDER BY c._ts ASC"
        };

        return this.cosmosClient.database(process.env.cosmosDbName)
            .container(MESSAGES_CONTAINER)
            .items.query(querySpec, {enableCrossPartitionQuery: true}).toArray();

    }

    /**
    * Read the database and get statistics
    */
    getStatistics(){
        const vm = this
        const lastSixMonths = getLastSixMonths()

        const totalQuery = {
            query: "SELECT VALUE COUNT(c.id) FROM c"
        }

        const fallbacksQuery = {
            query: "SELECT VALUE COUNT(c.id) FROM c WHERE c.document.intent = 'fallback'"
        }

        const perMonthQuery = {
            query: "SELECT VALUE COUNT(c.id) FROM c WHERE CONTAINS (c.document.timestamp, '@dateMonth')"
        }

        const fallbacksPerMonthQuery = {
            query: "SELECT VALUE COUNT(c.id) FROM c WHERE CONTAINS (c.document.timestamp, '@dateMonth') AND c.document.intent = 'fallback'"
        }

        const queryPromise = (container, query) => {
            return new Promise((resolve, reject) => {
                vm.cosmosClient.database(process.env.cosmosDbName)
                    .container(container)
                    .items
                    .query(query, {enableCrossPartitionQuery: true})
                    .toArray()
                    .then( (req) => {
                        resolve(req.result[0]);
                    }, (err) =>{
                        reject(err)
                    })
            })
        }

        const totalConversations = new Promise((resolve, reject) => {
            queryPromise(CHATROOMS_CONTAINER, totalQuery)
                .then( result => resolve({total_conversations: result}) );
        })

        const totalMessages = new Promise((resolve, reject) => {
            queryPromise(MESSAGES_CONTAINER, totalQuery)
                .then( result => resolve({total_messages: result}) );
        })

        const totalFallback = new Promise((resolve, reject) => {
            queryPromise(MESSAGES_CONTAINER, fallbacksQuery)
                .then( result => resolve({total_fallback_messages: result}) );
        })

        const conversationsPerMonth = new Promise((resolve, reject) => {
            const allConversationsInMonth = []
            lastSixMonths.forEach( (monthAndYear, index) => {
                const monthQuery = perMonthQuery.query.replace("@dateMonth", monthAndYear)
                queryPromise(CHATROOMS_CONTAINER, {query: monthQuery})
                    .then( result => {
                        allConversationsInMonth[index] = [
                            MONTHS[parseInt(monthAndYear.split('-')[1], 10) - 1],
                            result
                        ]
                        if (allConversationsInMonth.length == lastSixMonths.length) {
                          resolve({conversations_per_month: allConversationsInMonth })  
                        } 
                    });
            })
        })

        const messagesPerMonth = new Promise((resolve, reject) => {
            const allMessagesInMonth = []
            lastSixMonths.forEach( (monthAndYear, index) => {
                const monthQuery = perMonthQuery.query.replace("@dateMonth", monthAndYear)
                queryPromise(MESSAGES_CONTAINER, {query: monthQuery})
                    .then( result => {
                        allMessagesInMonth[index] = [
                            MONTHS[parseInt(monthAndYear.split('-')[1], 10) - 1],
                            result
                        ]
                        if (allMessagesInMonth.length == lastSixMonths.length) {
                          resolve({messages_per_month: allMessagesInMonth })  
                        } 
                    });
            })
        })

        const fallbacksPerMonth = new Promise((resolve, reject) => {
            const allFallbacksInMonth = []
            lastSixMonths.forEach( (monthAndYear, index) => {
                const monthQuery = fallbacksPerMonthQuery.query.replace("@dateMonth", monthAndYear)
                queryPromise(MESSAGES_CONTAINER, {query: monthQuery})
                    .then( result => {
                        allFallbacksInMonth[index] = [
                            MONTHS[parseInt(monthAndYear.split('-')[1], 10) - 1],
                            result
                        ]
                        if (allFallbacksInMonth.length == lastSixMonths.length) {
                          resolve({fallbacks_per_month: allFallbacksInMonth })  
                        } 
                    });
            })
        })

        return Promise.all([
            totalConversations,
            totalMessages,
            totalFallback,
            conversationsPerMonth,
            messagesPerMonth,
            fallbacksPerMonth
        ])
    }

};

module.exports.DbWrapper = DbWrapper;