// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.
const util = require('util');
const { QnAMaker, LuisRecognizer } = require('botbuilder-ai');
const { DbWrapper } = require('./db');

// Name of the QnA Maker service in the .bot file.
const QNA_CONFIGURATION = '46' // RoamingSmalltalk // 'roaming_smalltalk_emp_qa_kb';
const LUIS_CONFIGURATION = '34' // MovistarSmalltalk // 'RoamingSmalltalkQA';
// CONSTS used in QnA Maker query. See [here](https://docs.microsoft.com/en-us/azure/bot-service/bot-builder-howto-qna?view=azure-bot-service-4.0&tabs=cs) for additional info
const QNA_TOP_N = 1;
const QNA_CONFIDENCE_THRESHOLD = 0.5;

class SmalltalkQnA {
    /**
     *
     * @param {Object} botConfig bot configuration from .bot file
     */
    constructor(botConfig) {
        if (!botConfig) throw new Error('Need bot config');

        // add recognizers
        const qnaConfig = botConfig.findServiceByNameOrId(QNA_CONFIGURATION);
        if (!qnaConfig || !qnaConfig.kbId) throw new Error(`SmalltalkQnA QnA Maker application information not found in .bot file. Please ensure you have all required QnA Maker applications created and available in the .bot file. See readme.md for additional information\n`);
        this.qnaRecognizer = new QnAMaker({
            knowledgeBaseId: qnaConfig.kbId,
            endpointKey: qnaConfig.endpointKey,
            host: qnaConfig.hostname
        });

        const luisConfig = botConfig.findServiceByNameOrId(LUIS_CONFIGURATION);
        if (!luisConfig || !luisConfig.appId) throw new Error(`SmalltalkQnA LUIS model not found in .bot file. Please ensure you have all required LUIS models created and available in the .bot file. See readme.md for additional information\n`);
        this.luisRecognizer = new LuisRecognizer({
            applicationId: luisConfig.appId,
            azureRegion: luisConfig.region,
            // CAUTION: Its better to assign and use a subscription key instead of authoring key here.
            endpointKey: luisConfig.subscriptionKey
        });
        this.db = new DbWrapper(botConfig)
    }
    /**
     *
     * @param {TurnContext} turn context object
     */
    async onTurn(turnContext) {
        // Call QnA Maker and get results.
        const luisResults = await this.luisRecognizer.recognize(turnContext);
        const topIntent = LuisRecognizer.topIntent(luisResults);
        console.log("SmalltalkQnatopIntent", topIntent)
        const qnaResult = await this.qnaRecognizer.generateAnswer(topIntent.replace(/_/g, '.').replace(/-/g, '_'), QNA_TOP_N, QNA_CONFIDENCE_THRESHOLD);
        console.log("SmalltalkQnaResult", qnaResult)
        if (!qnaResult || qnaResult.length === 0 || !qnaResult[0].answer) {
            await turnContext.sendActivities([
                { type: 'typing' },
                { type: 'delay', value: 1000 },
                { type: 'message', text: `¡Oh! Aún no conozco la respuesta a esa pregunta, pero puedes encontrar más información en [nuestro sitio web de roaming](https://ww2.movistar.cl/telefonia-movil/roaming/)` },
                { type: 'message', text: `También puedes comunicarte con nosotros y hablar con uno de nuestros ejecutivos llamando al 611 desde Chile o al +56991610611 desde el extranjero.` }
            ]);
            this.db.createMessage(turnContext, 'fallback', '¡Oh! Aún no conozco la respuesta a esa pregunta, pero puedes encontrar más información en [nuestro sitio web de roaming](https://ww2.movistar.cl/telefonia-movil/roaming/)<br>También puedes comunicarte con nosotros y hablar con uno de nuestros ejecutivos llamando al 611 desde Chile o al +56991610611 desde el extranjero.')
            return;
        } else {
            const qnaAnswer = qnaResult[Math.floor(Math.random() * qnaResult.length)];
            await turnContext.sendActivities([
                { type: 'typing' },
                { type: 'delay', value: 1000 },
                { type: 'message', text: qnaAnswer.answer }
            ]);
            this.db.createMessage(turnContext, topIntent, qnaAnswer.answer)
            return;
        }

    }
};

module.exports.SmalltalkQnA = SmalltalkQnA;